const reverseStringRecursive = string => {
  if (string === '') return '';
  return (
    string.charAt(string.length - 1) +
    reverseStringRecursive(string.slice(0, string.length - 1))
  );
};

const reverseString = string => {
  let ans = '';
  for (let i = string.length - 1; i >= 0; i--) {
    ans += string[i];
  }
  return ans;
};

module.exports = {
  reverseStringRecursive,
  reverseString,
};
