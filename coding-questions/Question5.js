/**
 * Check if two Strings are anagrams of each other
 *
 * rats, star -> true.
 *
 */

const areAnagrams = (str1, str2) => {
  if (str1.length !== str2.length) return false;

  str1 = str1.toLowerCase();
  str2 = str2.toLowerCase();

  let cache = new Array(122).fill(0);
  for (let i = 0; i < str1.length; i++) {
    cache[str1.charCodeAt(i)]++;
  }

  for (let i = 0; i < str2.length; i++) {
    cache[str2.charCodeAt(i)]--;
  }

  cache.forEach(val => {
    if (val !== 0) {
      return false;
    }
  });
  return true;
};

module.exports = {
  areAnagrams,
};
