/**
 * Largest and smallest number in an unsorted integer array
 */

const getMaxAndMin = arr => {
  if (!arr.length) {
    return [-1, -1];
  }
  let max = arr[0],
    min = arr[0];
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < min) {
      min = arr[i];
    }
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return [min, max];
};

module.exports = {
  getMaxAndMin,
};
