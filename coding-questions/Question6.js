/**
 *
 * Count the occurrence of a given character in String
 */

const countOccurence = (string, letter) => {
  if (!string || !letter) {
    return 0;
  }

  // Process input for consistency
  string = string.toLowerCase();
  letter = letter.toLowerCase();
  let occurrence = 0;
  string.split('').forEach(char => {
    if (char === letter) {
      occurrence++;
    }
  });
  return occurrence;
};

module.exports = {
  countOccurence,
};
