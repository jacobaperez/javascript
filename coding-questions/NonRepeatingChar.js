/**
 * Print first non repeated character from String
 */

const firstNonRepeatingChar = string => {
  let cache = {};
  for (let i = 0; i < string.length; i++) {
    if (cache[string[i]]) {
      return string[i];
    } else {
      cache[string[i]] = true;
    }
  }
  return -1;
};

exports.firstNonRepeatingChar = firstNonRepeatingChar;
