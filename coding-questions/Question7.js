/**
 * Given an array of length n+1,
 * whose elements belong to 1->n
 * there must exist a duplicate.
 * find in linear time and space.
 *
 */

const findDuplicate = arr => {
  let cache = {};
  for (let i = 0; i < arr.length; i++) {
    if (cache[arr[i]]) {
      return arr[i];
    } else {
      cache[arr[i]] = true;
    }
  }
};

module.exports = {
  findDuplicate,
};
