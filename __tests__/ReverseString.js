const {
  reverseStringRecursive,
  reverseString,
} = require('../coding-questions/ReverseString');

describe('Test Reverse String Recursive', () => {
  it('Empty String should return empty', () => {
    let res = reverseStringRecursive('');
    expect(res).toEqual('');
  });

  it('hello -> olleh', () => {
    let res = reverseStringRecursive('hello');
    expect(res).toEqual('olleh');
  });

  it('abc123:( -> (:321cba', () => {
    let res = reverseStringRecursive('abc123:(');
    expect(res).toEqual('(:321cba');
  });
});

describe('Test Reverse String Iteratively', () => {
  it('Empty String should return empty', () => {
    let res = reverseString('');
    expect(res).toEqual('');
  });

  it('hello -> olleh', () => {
    let res = reverseString('hello');
    expect(res).toEqual('olleh');
  });

  it('abc123:( -> (:321cba', () => {
    let res = reverseString('abc123:(');
    expect(res).toEqual('(:321cba');
  });
});
