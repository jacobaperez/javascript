const { areAnagrams } = require('../coding-questions/Question5');

describe('Check if two strings are anagrams of each other', () => {
  it('Should return true for two empty strings', () => {
    expect(areAnagrams('', '')).toEqual(true);
  });

  it('should return false for different string lengths', () => {
    expect(areAnagrams('asdf', 'as')).toEqual(false);
  });

  it('should return true for anagrams', () => {
    expect(areAnagrams('asdfg', 'gfdsa')).toEqual(true);
  });
});
