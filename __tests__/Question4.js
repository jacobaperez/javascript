const { getMaxAndMin } = require('../coding-questions/Question4');

describe('Get Max and Min from an unsorted array', () => {
  it('should return [-1,1] for empty aray', () => {
    expect(getMaxAndMin([])).toEqual([-1, -1]);
  });

  it('[1,2,3,4,5,-3,2,11,-5,0,3] -> [-5, 11]', () => {
    expect(getMaxAndMin([1, 2, 3, 4, 5, -3, 2, 11, -5, 0, 3])).toEqual([
      -5,
      11,
    ]);
  });
});
