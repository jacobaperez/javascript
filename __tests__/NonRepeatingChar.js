const {
  firstNonRepeatingChar,
} = require('../coding-questions/NonRepeatingChar');

describe('First non repeating Char', () => {
  it('should return -1 for all unique', () => {
    let res = firstNonRepeatingChar('abcdefghijklmnopqrstuvwxyz');
    expect(res).toEqual(-1);
  });

  it('abcfcba-> c', () => {
    let res = firstNonRepeatingChar('abcfcba');
    expect(res).toEqual('c');
  });
});
