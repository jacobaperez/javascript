/**
 *
 * @param {Array} arr - array to be sorted
 */
const MergeSort = arr => {
  if (arr.length <= 1) {
    return arr;
  }
  let mid = Math.floor(arr.length / 2);
  let left = arr.slice(0, mid);
  let right = arr.slice(mid);
  return Merge(MergeSort(left), MergeSort(right));
};


/**
 * 
 * @param {Array} left left sub array
 * @param {Array} right right sub array
 */
const Merge = (left, right) => {
  let res = [];
  while (left.length && right.length) {
    if (left[0] <= right[0]) {
      res.push(left.shift());
    } else {
      res.push(right.shift());
    }
  }
  return res.concat(left, right);
};

let a = [2,5,4,3,7,9,1,10]