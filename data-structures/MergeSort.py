import math


# Attempt at MergeSort in Python
def MergeSort(arr):
    if len(arr) <= 1:
        return arr
    else:
        mid = math.floor(len(arr)/2)
        left = arr[0:mid]
        right = arr[mid:]
        return Merge(MergeSort(left), MergeSort(right))


# Merge routine
def Merge(left, right):
    res = []
    while len(left) != 0 and len(right) != 0:
        if left[0] <= right[0]:
            res.append(left.pop(0))
        else:
            res.append(right.pop(0))
    res[len(res):] = left
    res[len(res):] = right
    return res


a = [5, 8, 3, 7, 4, 8, 1, 2, 10]
print('Unsorted: ', a)
print('Sorted: ', MergeSort(a))
