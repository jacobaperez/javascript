/**
 *
 * @param {Array} arr - Array to be sorted
 */
function Quicksort(arr) {
  if (arr.length <= 1) {
    return arr;
  }
  let pivot = arr[0];
  let left = [],
    mid = [pivot],
    right = [];
  for (let i = 1; i < arr.length; i++) {
    let val = arr[i];
    if (val === pivot) {
      mid.push(val);
    } else if (val < pivot) {
      left.push(val);
    } else {
      right.push(val);
    }
  }
  return Quicksort(left).concat(mid, Quicksort(right));
}

let a = [8, 3, 7, 4, 6, 5, 1, 9, 10];
