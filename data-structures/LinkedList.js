class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
  }

  append(val) {
    if (!this.head) {
      this.head = new Node(val);
      this.tail = this.head;
    } else {
      this.tail.next = new Node(val);
      this.tail = this.tail.next;
    }
    this.print();
  }

  prepend(val) {
    if (!this.head) {
      this.head = new Node(val);
      this.tail = this.head;
    } else {
      let node = new Node(val);
      node.next = this.head;
      this.head = node;
    }
    this.print();
  }

  print() {
    let str = "",
      head = this.head;
    while (head) {
      str += head.val + " -> ";
      head = head.next;
    }
    str += "null";
    console.log(str);
  }

  sort() {
    if (!this.head) {
      return -1;
    }
    let arr = [],
      head = this.head;
    while (head) {
      arr.push(head.val);
      head = head.next;
    }
    arr.sort((a, b) => a - b);
    this.head = new Node(arr.shift());
    this.tail = this.head;
    while (arr.length) {
        this.append(arr.shift());
    }
  }
}
